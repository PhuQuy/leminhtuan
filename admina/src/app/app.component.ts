import { Component } from '@angular/core';
import { Router, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  is_root = false;
  constructor(private router: Router) {
    this.router.events.subscribe((event) => {
      if (router.url === '') {
        this.is_root = false;
      } else if (router.url === '/login') {
        this.is_root = true;
      } else {
        this.is_root = false;

      }
    });
  }

}
