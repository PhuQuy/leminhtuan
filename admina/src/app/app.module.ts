import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { environment } from '@environments/environment';
import { AuthGuard } from './core/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: './routes/dashboard/dashboard.module#DashboardModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'apartment',
    loadChildren: './routes/apartment/apartment.module#ApartmentModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'user',
    loadChildren: './routes/user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: './routes/login-page/login-page.module#LoginPageModule'
  },
  {
    path: 'bang-hang',
    loadChildren: './routes/bang-hang/bang-hang.module#BangHangModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'project',
    loadChildren: './routes/project/project.module#ProjectModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'task',
    loadChildren: './routes/task/task.module#TaskModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'lien-he',
    loadChildren: './routes/contact/contact.module#ContactModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [AuthGuard, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
