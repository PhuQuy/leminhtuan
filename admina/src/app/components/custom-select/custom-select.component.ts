import { Component, AfterViewInit, Output, EventEmitter, Input } from '@angular/core';

declare var $: any;
@Component({
  selector: 'custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.scss']
})
export class CustomSelectComponent implements AfterViewInit {

  @Output() changeOption = new EventEmitter<string>();
  @Input() currentSelect: any;
  @Input() options: any;
  datas: any;
  constructor() {

  }

  changeProject() {
    this.changeOption.emit(this.currentSelect);
  }

  ngAfterViewInit() {
    this.options.subscribe(options => {
      this.datas = options;
      $("document").ready(function() {
        $('.custom-selectpicker').selectpicker();
      });
    })
  }

}
