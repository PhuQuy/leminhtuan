import { Component, OnInit } from '@angular/core';
import { ApartmentService } from '@services/apartment.service';
import { ActivatedRoute } from "@angular/router";
import { TransferService } from '@services/transfer.service';
import { UploadService } from '@services/upload.service';
import { ProjectService } from '@services/project.service';
import { RentService } from '@services/rent.service';

declare var $: any;
@Component({
  selector: 'app-apartment-details',
  templateUrl: './apartment-details.component.html',
  styleUrls: ['./apartment-details.component.scss'],
  providers: [UploadService, ProjectService]
})
export class ApartmentDetailsComponent implements OnInit {
  apartment: any = new Object();
  id: string;
  transfer: any = {
    details: {},
    location: {},
    images: [],
    apartmentCode: [],
    videos: {}
  }
  rent: any = {
    location: {},
    images: []
  }
  apartmentCode: any;
  isCreated: boolean = false;
  projects: any;

  constructor(protected apartmentService: ApartmentService, private activatedRoute: ActivatedRoute, protected transferService: TransferService,
    private upSvc: UploadService, protected projectService: ProjectService, protected rentService: RentService) {
    this.activatedRoute.params.subscribe(params => {
      if (params['key']) {
        this.id = params['key'];
        apartmentService.getById(params['key']).subscribe(apartment => {
          $("document").ready(function() {
            $('.datetimepicker').datetimepicker();
          });
          if (!apartment) {
            this.isCreated = true;
            return;
          }
          this.apartment = apartment;
          if (this.apartment.type === 'chuyen-nhuong') {
            transferService.getById(this.id).subscribe(transfer => {
              if (!transfer) { return }
              this.transfer = transfer;
            });
          } else {
            rentService.getById(this.id).subscribe(rent => {
              if (!rent) {
                return;
              } else {
                this.rent = rent;
              }
            })
          }
        });
      }
    });

    this.projects = projectService.getAll();
  }

  loadProject(event) {
    this.apartment.duan = event;
  }

  update() {
    if (this.isCreated) {
      this.id = this.apartmentService.getSlug(this.apartment.name);
    }
    this.apartment.furniture = this.rent.furniture;
    this.apartmentService.updateWithId(this.apartment, this.id).then(val => {
    }, error => {
      this.showNotification('top', 'center', error, 'danger');
    });

    if (this.apartment.type == 'chuyen-nhuong') {
      this.transferService.updateWithId(this.transfer, this.id).then(val => {
        this.showNotification('top', 'center', "Update Success", 'success');
      }, error => {
        this.showNotification('top', 'center', error, 'danger');
      });
    } else {
      this.rentService.updateWithId(this.rent, this.id).then(val => {
        this.showNotification('top', 'center', "Update Success", 'success');
      }, error => {
        this.showNotification('top', 'center', error, 'danger');
      });
    }
  }

  showNotification(from, align, message, type) {

    $.notify({
      icon: "now-ui-icons ui-1_bell-53",
      message: message

    }, {
        type: type,
        timer: 50,
        placement: {
          from: from,
          align: align
        },
      });
  }

  detectPhotos(event) {
    const that = this;
    if (event.target.files) {
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.upSvc.pushUpload(files.item(i)).subscribe(uploaded => {
          this.transfer.images.push(uploaded);
          if (!that.isCreated) {
            this.update();
          }
        })
      }
    }
  }

  detectPhoto(event) {
    const that = this;
    if (event.target.files) {
      let files = event.target.files;
      this.upSvc.pushUpload(files.item(0)).subscribe(uploaded => {
        this.apartment.image = uploaded;
        if (!that.isCreated) {
          this.update();
        }
      })
    }
  }

  detectRentPhotos(event) {
    const that = this;
    if (event.target.files) {
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.upSvc.pushUpload(files.item(i)).subscribe(uploaded => {
          this.rent.images.push(uploaded);
          if (!that.isCreated) {
            this.update();
          }
        })
      }
    }
  }

  deletePhoto(imagePath) {
    if (imagePath) {
      let name = imagePath.substr(imagePath.indexOf('%2F') + 3, (imagePath.indexOf('?')) - (imagePath.indexOf('%2F') + 3));
      this.transfer.images.splice(this.transfer.images.indexOf(imagePath), 1);
      this.update();
      this.upSvc.deleteFileStorage(name);
    }
  }

  deleteRentPhoto(imagePath) {
    if (imagePath) {
      let name = imagePath.substr(imagePath.indexOf('%2F') + 3, (imagePath.indexOf('?')) - (imagePath.indexOf('%2F') + 3));
      this.rent.images.splice(this.rent.images.indexOf(imagePath), 1);
      this.update();
      this.upSvc.deleteFileStorage(name);
    }
  }

  addCode(event: any) {
    this.transfer.apartmentCode.push(event.target.value);
    event.target.value = '';
  }

  ngOnInit() {

  }

}
