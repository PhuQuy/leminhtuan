import { Component, Input } from '@angular/core';
import { ApartmentService } from '@services/apartment.service';

declare var $: any;
declare var swal: any;
@Component({
  selector: 'apartment-list',
  templateUrl: './apartment-list.component.html',
  styleUrls: ['./apartment-list.component.scss']
})
export class ApartmentListComponent {
  apartments: any;
  @Input() showTotal: boolean;
  constructor(protected apartmentService: ApartmentService) {

    apartmentService.getAll().subscribe(apartments => {
      this.apartments = apartments;
      $("document").ready(function() {
        $('.selectpicker').selectpicker();
      });
    });
  }

  delete(id: any) {
    const that = this;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then(function() {
      that.apartmentService.delete(id).then(() => {
        swal({
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          showConfirmButton: !1,
          timer: 1000,
        });
      });
    }).catch(swal.noop);
  }

  ngOnInit() {
  }

}
