import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ApartmentListModule } from '@routes/apartment/apartment-list/apartment-list.module';
import { ApartmentComponent } from './apartment.component';
import { ApartmentDetailsComponent } from './apartment-details/apartment-details.component';
import { ApartmentService } from '@services/apartment.service';
import { TransferService } from '@services/transfer.service';
import { CustomSelectModule } from '@components/custom-select/custom-select.module';
import { RentService } from '@services/rent.service';

const routes: Routes = [
  {
    path: '', component: ApartmentComponent
  },

  {
    path: ':key', component: ApartmentDetailsComponent
  },

  {
    path: 'create', component: ApartmentDetailsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ApartmentListModule,
    CustomSelectModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ApartmentComponent, ApartmentDetailsComponent],
  exports: [ApartmentComponent],
  providers: [ApartmentService, TransferService, RentService]
})
export class ApartmentModule { }
