import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { BanghangService } from '@services/banghang.service';
import { ProjectService } from '@services/project.service';

declare var $: any;
@Component({
  selector: 'bang-hang-details',
  templateUrl: './bang-hang-details.component.html',
  styleUrls: ['./bang-hang-details.component.scss'],
  providers: [BanghangService, ProjectService]
})
export class BangHangDetailsComponent implements OnInit {

  bangHang: any = new Object();
  id: string;
  projects: any;
  constructor(private activatedRoute: ActivatedRoute, private bangHangService: BanghangService, protected projectService: ProjectService) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['key'];
      if (this.id) {
        bangHangService.getById(this.id).subscribe(bangHang => {
          if (bangHang) {
            this.bangHang = bangHang;
          }
        });
      }
    });

    this.projects = projectService.getAll();
  }

  loadProject(event) {
    this.bangHang.duan = event;
  }

  updateOrCreate() {
    if(this.bangHang.id){
      this.bangHangService.updateWithId(this.bangHang, this.id).then(val => {
        this.showNotification('top', 'center', "Update Success", 'success');
      }, error => {
        this.showNotification('top', 'center', error, 'danger');
      });
    } else {
      this.bangHangService.create(this.bangHang).then( ()=> {
        this.showNotification('top', 'center', "Update Success", 'success');
      }, error => {
        this.showNotification('top', 'center', error, 'danger');
      });
    }
  }

  showNotification(from, align, message, type) {

    $.notify({
      icon: "now-ui-icons ui-1_bell-53",
      message: message

    }, {
        type: type,
        timer: 50,
        placement: {
          from: from,
          align: align
        },
      });
  }

  ngOnInit() {
  }

}
