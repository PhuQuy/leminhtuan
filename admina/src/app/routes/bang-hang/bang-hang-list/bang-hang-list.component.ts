import { Component, OnInit, Input } from '@angular/core';
import { BanghangService } from '@services/banghang.service';

declare var swal: any;
@Component({
  selector: 'bang-hang-list',
  templateUrl: './bang-hang-list.component.html',
  styleUrls: ['./bang-hang-list.component.scss'],
  providers: [BanghangService]
})
export class BangHangListComponent {


  bangHangs: any;
  @Input() showTotal: boolean;
  constructor(protected bangHangService: BanghangService) {
    bangHangService.getAll().subscribe(bangHangs => {
      this.bangHangs = bangHangs;
    })
  }

  delete(id: any) {
    const that = this;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then(function() {
      that.bangHangService.delete(id).then(() => {
        swal({
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          showConfirmButton: !1,
          timer: 1000,
        });
      });
    }).catch(swal.noop);
  }

  ngOnInit() {
  }

}
