import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BangHangListComponent } from './bang-hang-list.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [BangHangListComponent],
  exports: [BangHangListComponent]
})
export class BangHangListModule { }
