import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BangHangComponent } from './bang-hang.component';
import { Routes, RouterModule } from '@angular/router';
import { BangHangDetailsComponent } from './bang-hang-details/bang-hang-details.component';
import { BangHangListModule } from '@routes/bang-hang/bang-hang-list/bang-hang-list.module';
import { FormsModule } from '@angular/forms';
import { CustomSelectModule } from '@components/custom-select/custom-select.module';

const routes: Routes = [
  {
    path: '', component: BangHangComponent
  },
  {
    path: ':key', component: BangHangDetailsComponent
  },
  {
    path: 'create', component: BangHangDetailsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    BangHangListModule,
    FormsModule,
    CustomSelectModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BangHangComponent, BangHangDetailsComponent]
})
export class BangHangModule { }
