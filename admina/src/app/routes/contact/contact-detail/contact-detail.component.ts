import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ContactService } from '@services/contact.service';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss'],
  providers: [ContactService]
})
export class ContactDetailComponent implements OnInit {

  contact: any;
  id: string;
  constructor(private activatedRoute: ActivatedRoute, private contactService: ContactService) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['key'];
      if (this.id) {
        contactService.getById(this.id).subscribe(contact => {
            this.contact = contact;
        });
      }
    });
  }

  ngOnInit() {
  }

}
