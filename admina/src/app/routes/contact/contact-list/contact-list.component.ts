import { Component, OnInit } from '@angular/core';
import { ContactService } from '@services/contact.service';

declare var swal: any;
@Component({
  selector: 'contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
  providers: [ContactService]
})
export class ContactListComponent implements OnInit {

  contacts:any;
  constructor(protected contactService: ContactService) {
    contactService.getAll().subscribe(contacts =>{
      this.contacts = contacts;
    })
  }

  delete(id: any) {
    const that = this;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then(function() {
      that.contactService.delete(id).then(() => {
        swal({
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          showConfirmButton: !1,
          timer: 1000,
        });
      });
    }).catch(swal.noop);
  }

  ngOnInit() {
  }

}
