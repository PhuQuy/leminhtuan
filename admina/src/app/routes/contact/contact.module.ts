import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactListModule } from '@routes/contact/contact-list/contact-list.module';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '', component: ContactComponent
  },
  {
    path: ':key', component: ContactDetailComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    ContactListModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactComponent, ContactDetailComponent]
})
export class ContactModule { }
