import { Component, OnInit } from '@angular/core';
import { UserService } from '@services/user.service';
import { ApartmentService } from '@services/apartment.service';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [UserService, ApartmentService]
})
export class DashboardComponent implements OnInit {
  users: any;
  apartments: any;
  constructor(private userService: UserService, protected apartmentService: ApartmentService) {
    userService.getAll().subscribe(users => {
      this.users = users;
    });
    apartmentService.getAll().subscribe(apartments => {
      this.apartments = apartments;
      $("document").ready(function() {
        $('.selectpicker').selectpicker();
      });
    });
  }

  ngOnInit() {

  }

}
