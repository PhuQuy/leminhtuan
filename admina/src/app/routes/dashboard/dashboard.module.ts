import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ApartmentListModule } from '@routes/apartment/apartment-list/apartment-list.module';
import { DashboardComponent } from './dashboard.component';
import { UserListModule } from '@routes/user/user-list/user-list.module';
import { TaskListModule } from '@routes/task/task-list/task-list.module';
import { ProjectListModule } from '@routes/project/project-list/project-list.module';

const routes: Routes = [
  {
    path: '', component: DashboardComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ApartmentListModule,
    UserListModule,
    TaskListModule,
    ProjectListModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
