import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  providers: [AngularFireAuth]
})
export class LoginPageComponent implements OnInit {
  public showSpinner = false;
  public error: any;
  public email: string;
  public password: string;
  constructor(public afAuth: AngularFireAuth, public router: Router) { }

  ngOnInit() {
  }

  loginByEmailAndPassword() {
    this.showSpinner = true;
    this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password).then(
      (success) => {
        this.error = null;
        this.showSpinner = false;
        this.router.navigate(['/']);
      }).catch(
        (err) => {
          this.showSpinner = false;
          this.error = err.message;
        });
  }

}
