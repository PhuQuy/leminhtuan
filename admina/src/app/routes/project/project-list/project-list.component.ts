import { Component, OnInit, Input } from '@angular/core';
import { ProjectService } from '@services/project.service';

declare var swal: any;
@Component({
  selector: 'project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
  projects: any;
  @Input() showTotal: boolean;
  constructor(protected projectService: ProjectService) {
    projectService.getAll().subscribe(projects => {
      this.projects = projects;
    })
  }

  delete(id: any) {
    const that = this;
    swal({
      title: 'Are you sure?',
      text: "Bạn có thực sự muốn xóa dự án này?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then(function() {
      that.projectService.delete(id).then(() => {
        swal({
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          showConfirmButton: !1,
          timer: 1000,
        });
      });
    }).catch(swal.noop);
  }

  ngOnInit() {
  }

}
