import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ProjectListComponent } from './project-list.component';
import { ProjectService } from '@services/project.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [ProjectListComponent],
  providers:[ProjectService],
  exports:[ProjectListComponent]
})
export class ProjectListModule { }
