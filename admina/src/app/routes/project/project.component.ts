import { Component } from '@angular/core';
import { ProjectService } from '@services/project.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent {
  project:any = new Object();
  id: string;
  isCreate: boolean = true;
  constructor(protected projectService: ProjectService, private activatedRoute: ActivatedRoute,) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['key'];
      if (this.id) {
        this.isCreate = false;
        projectService.getById(this.id).subscribe(project => {
          if(project) {
            this.project = project;
          }
        });
      } else {
        this.isCreate = true;
      }
    });

  }

  ngOnInit() {
  }

  updateOrCreate() {
    this.project.id = this.projectService.getSlug(this.project.name);
    this.projectService.updateOrCreate(this.project).then( ()=> {
      console.log('Create success')
    }, error => {
      console.log('Create unsuccess')
    });
  }

}
