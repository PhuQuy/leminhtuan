import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ProjectComponent } from './project.component';
import { ProjectService } from '@services/project.service';
import { ProjectListModule } from './project-list/project-list.module';
import { ProjectLsComponent } from './project-ls/project-ls.component';

const routes: Routes = [
  {
    path: '', component: ProjectLsComponent
  },
  {
    path: ':key', component: ProjectComponent
  },
  {
    path: 'create', component: ProjectComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProjectListModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProjectComponent, ProjectLsComponent],
  providers:[ProjectService]
})
export class ProjectModule { }
