import { Component, OnInit } from '@angular/core';
import { TaskService } from '@services/task.service';
import { ActivatedRoute } from "@angular/router";

declare var $: any;
@Component({
  selector: 'task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss'],
  providers: [TaskService]
})
export class TaskDetailComponent implements OnInit {

  task: any = new Object();
  id: string;

  constructor(private activatedRoute: ActivatedRoute, protected taskService: TaskService) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['key'];
      if (this.id) {
        taskService.getById(this.id).subscribe(task => {
          if (task) {
            this.task = task;
          }
        });
      }
    });
  }

  showNotification(from, align, message, type) {

    $.notify({
      icon: "now-ui-icons ui-1_bell-53",
      message: message

    }, {
        type: type,
        timer: 50,
        placement: {
          from: from,
          align: align
        },
      });
  }

  ngOnInit() {
  }

  updateOrCreate() {
    if(!this.task.id){
      this.task.status = false;
    }
    this.task.id = this.taskService.getSlug(this.task.title);
    this.taskService.updateOrCreate(this.task).then( ()=> {
      this.showNotification('top', 'center', "Update Success", 'success');
    }, error => {
      this.showNotification('top', 'center', error, 'danger');
    });
  }
}
