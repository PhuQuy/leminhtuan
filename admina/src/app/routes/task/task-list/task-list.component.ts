import { Component, OnInit } from '@angular/core';
import { TaskService } from '@services/task.service';

declare var swal: any;
declare var $: any;
@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  providers: [TaskService]
})
export class TaskListComponent implements OnInit {

  tasks: any;
  constructor(protected taskService: TaskService) {
    taskService.getAll().subscribe(tasks => {
      this.tasks = tasks;
    })
  }

  delete(id: any) {
    const that = this;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then(function() {
      that.taskService.delete(id).then(() => {
        swal({
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          showConfirmButton: !1,
          timer: 1000,
        });
      });
    }).catch(swal.noop);
  }
  ngOnInit() {
  }

  showNotification(from, align, message, type) {

    $.notify({
      icon: "now-ui-icons ui-1_bell-53",
      message: message

    }, {
        type: type,
        timer: 50,
        placement: {
          from: from,
          align: align
        },
      });
  }

  change(task: any){
    this.taskService.updateWithId(task, task.id).then(val => {
      this.showNotification('top', 'center', "Update Success", 'success');
    }, error => {
      this.showNotification('top', 'center', error, 'danger');
    });
  }
}
