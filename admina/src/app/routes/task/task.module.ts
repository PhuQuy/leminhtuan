import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TaskComponent } from './task.component';
import { Routes, RouterModule } from '@angular/router';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskListModule } from '@routes/task/task-list/task-list.module';

const routes: Routes = [
  {
    path: '', component: TaskComponent
  },
  {
    path: ':key', component: TaskDetailComponent
  },
  {
    path: 'create', component: TaskDetailComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TaskListModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TaskComponent, TaskDetailComponent]
})
export class TaskModule { }
