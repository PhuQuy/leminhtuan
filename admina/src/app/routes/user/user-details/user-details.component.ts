import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { UserService } from '@services/user.service';

declare var $: any;
@Component({
  selector: 'user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  providers: [UserService]
})
export class UserDetailsComponent {
  user: any;
  id: string;
  constructor(private activatedRoute: ActivatedRoute, private userService: UserService) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['key'];
      if (params['key']) {
        userService.getById(this.id).subscribe(user => {
          this.user = user;
        });
      }

    });

  }

  updateUser() {
    this.userService.updateWithId(this.user, this.id).then(val => {
      this.showNotification('top', 'center', "Update Success", 'success');
    }, error => {
      this.showNotification('top', 'center', error, 'danger');
    });
  }

  showNotification(from, align, message, type) {

    $.notify({
      icon: "now-ui-icons ui-1_bell-53",
      message: message

    }, {
        type: type,
        timer: 50,
        placement: {
          from: from,
          align: align
        },
      });
  }

  ngOnInit() {
  }

}
