import { Component, Input } from '@angular/core';
import { UserService } from '@services/user.service';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [UserService]
})
export class UserListComponent {

  users: any;
  @Input() showTotal: boolean;
  constructor(private userService : UserService) {
    userService.getAll().subscribe(users => {
      this.users = users;
    })
  }

  ngOnInit() {
  }

}
