import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailsComponent } from '@routes/user/user-details/user-details.component';
import { FormsModule } from '@angular/forms';
import { UserListModule } from '@routes/user/user-list/user-list.module';

const routes: Routes = [
  {
    path: '', component: UserComponent
  },
  {
    path: ':key', component: UserDetailsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserListModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserComponent, UserDetailsComponent]
})
export class UserModule { }
