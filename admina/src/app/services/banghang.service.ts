import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { BaseService } from './base-service.service';

@Injectable()
export class BanghangService extends BaseService {

  constructor(protected angularFirestore: AngularFirestore) {
    super(angularFirestore, 'bang-hang-chuyen-nhuong');
  }

}
