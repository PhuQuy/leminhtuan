import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Injectable()
export class BaseService {
  protected basePath = '';
  constructor(protected angularFirestore: AngularFirestore, path: string) {
    this.basePath = path;
  }

  public getAll(query?) {
    return this.angularFirestore.collection<any>(this.basePath).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  public getById(id) {
    let itemPath = `${this.basePath}/${id}`;
    return this.angularFirestore.doc<any>(itemPath).valueChanges();
  }

  public getByElement(element:string, value:any) {
    return this.angularFirestore.collection(this.basePath, ref => ref.where(element, '==', value)).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as any;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  public create(data) {
    const timestamp = this.timestamp;
    return this.angularFirestore.collection(this.basePath).add({
      ...data, createdAt: timestamp
    });
  }

  public updateOrCreate(data) {
    if(data.id == null) {
      return null;
    }
    const timestamp = this.timestamp;
    return this.angularFirestore.collection(this.basePath).doc(data.id).set({
      ...data, createdAt: timestamp
    });
  }

  public delete(id) {
    console.log('d ' + id);
    return this.angularFirestore.collection(this.basePath).doc(id).delete();
  }

  public updateWithId(data, id) {
    if(id == null) {
      return null;
    }
    const timestamp = this.timestamp;
    return this.angularFirestore.collection(this.basePath).doc(id).set({
      ...data, createdAt: timestamp
    });
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  public getSlug(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str.replace(/\s+/g, '-');
  }

}
