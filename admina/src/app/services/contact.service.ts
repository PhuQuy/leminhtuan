import { Injectable } from '@angular/core';
import { BaseService } from './base-service.service';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class ContactService extends BaseService{

  constructor(protected angularFirestore: AngularFirestore) {
    super(angularFirestore, 'lien-he');
  }

}
