import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { BaseService } from './base-service.service';

@Injectable()
export class RentService extends BaseService{

  constructor(protected angularFirestore: AngularFirestore) {
    super(angularFirestore, 'cho-thue');
  }

}
