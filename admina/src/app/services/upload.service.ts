import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { Observer } from "rxjs/Observer";

@Injectable()
export class UploadService {
  basePath = 'uploads';
  constructor() { }

  pushUpload(upload: File) {
    return Observable.create((obs: Observer<String>) => {
      const storageRef = firebase.storage().ref();
      const uploadTask = storageRef.child(`${this.basePath}/${upload.name}`).put(upload);
      const sub = uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          // upload in progress
          const snap = snapshot as firebase.storage.UploadTaskSnapshot;
        },
        (error) => {
          // upload failed
          console.log(error)
        },
        () => {
          obs.next(uploadTask.snapshot.downloadURL);
          obs.complete();
        }
      );
    });
  }

  deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete()
  }
}
