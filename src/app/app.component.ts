import { Component } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public is_root = false;

  constructor(private router: Router) {
  this.router.events.subscribe((event) => {
    if(router.url === '/') {
      this.is_root = true;
    } else {
      this.is_root = false;
    }
  });
}
}
