import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterGroundComponent } from './components/footer-ground/footer-ground.component';
import { SignInComponent } from '@routes/user/sign-in/sign-in.component';

import { environment } from '@environments/environment';

const routes: Routes = [
  {
    path: '',
    loadChildren: './routes/home/home.module#HomeModule'
  },
  {
    path: 'cho-thue',
    loadChildren: './routes/rent/rent.module#RentModule'
  },
  {
    path: 'chuyen-nhuong',
    loadChildren: './routes/transfer-apartment/transfer-apartment.module#TransferApartmentModule'
  },
  {
    path: 'dang-ky',
    loadChildren: './routes/user/account.module#AccountModule'
  },
  {
    path: 'lien-he',
    loadChildren: './routes/contact-us/contact-us.module#ContactUsModule'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FooterGroundComponent,
    SignInComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
