import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'apartment-for-rent',
  templateUrl: './apartment-for-rent.component.html',
  styleUrls: ['./apartment-for-rent.component.css']
})
export class ApartmentForRentComponent implements OnInit {
  @Input() apartment: any;
  constructor() { }

  ngOnInit() {
  }

}
