import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ApartmentForRentComponent } from './apartment-for-rent.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [ApartmentForRentComponent],
  exports: [ApartmentForRentComponent]
})
export class ApartmentForRentModule { }
