import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'apartment-for-sale',
  templateUrl: './apartment-for-sale.component.html',
  styleUrls: ['./apartment-for-sale.component.css']
})
export class ApartmentForSaleComponent implements OnInit {
  @Input() apartment: any;
  constructor() { }

  ngOnInit() {
  }

}
