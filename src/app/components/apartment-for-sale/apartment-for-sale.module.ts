import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ApartmentForSaleComponent } from './apartment-for-sale.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [ApartmentForSaleComponent],
  exports: [ApartmentForSaleComponent]
})
export class ApartmentForSaleModule { }
