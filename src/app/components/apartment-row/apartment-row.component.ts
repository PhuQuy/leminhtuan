import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';
import { SharedService } from '@services/shared/shared.service';

@Component({
  selector: 'apartment-row',
  templateUrl: './apartment-row.component.html',
  styleUrls: ['./apartment-row.component.css']
})
export class ApartmentRowComponent implements OnInit {
  @Input() apartmentRows: any;
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;

  public items = [
    "assets/img/chungtu2.png",
    "assets/img/chungtu3.png",
    "assets/img/chungtu4.png"
  ];
  @ViewChild('prevButton') prevButton: ElementRef;
  @ViewChild('nextButton') nextButton: ElementRef;
  apartmentRow: any;
  constructor(protected sharedService: SharedService) {
    this.carouselTile = {
      grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
      slide: 1,
      speed: 400,
      interval: 2000,
      animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true,
      easing: 'ease'
    }
  }

  ngOnInit() {
    // this.carouselTileItems = this.items;
  }

  detail(data) {
    this.apartmentRow = data;
  }

  prev() {
    this.prevButton.nativeElement.click();
  }

  next() {
    this.nextButton.nativeElement.click();
  }

}
