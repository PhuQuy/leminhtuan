import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'component-apartment',
  templateUrl: './apartment.component.html',
  styleUrls: ['./apartment.component.css']
})
export class ApartmentComponent implements OnInit {
  @Input() apartment: any;

  constructor() { }

  ngOnInit() {
  }

}
