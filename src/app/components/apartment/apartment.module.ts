import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ApartmentComponent } from './apartment.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [ApartmentComponent],
  exports: [ApartmentComponent]
})
export class ApartmentModule { }
