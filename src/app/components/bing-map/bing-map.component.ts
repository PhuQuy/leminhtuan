import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bing-map',
  templateUrl: './bing-map.component.html',
  styleUrls: ['./bing-map.component.css']
})
export class BingMapComponent implements OnInit {
  @Input() title: string = 'Địa chỉ';
  @Input() lat: number = 12.2702649;
  @Input() lng: number = 109.2021127;
  @Input() zoom: number = 20;
  constructor() { }

  ngOnInit() {
  }

}
