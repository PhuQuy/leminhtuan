import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BingMapComponent } from './bing-map.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
     apiKey: 'AIzaSyAexgpCnpx59TFb6iPcSxkzNulpYvd3CkU'
   })
  ],
  declarations: [BingMapComponent],
  exports: [BingMapComponent]
})
export class BingMapModule { }
