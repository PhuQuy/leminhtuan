import { Component, AfterViewInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.css']
})
export class DateTimePickerComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit() {
    $("document").ready(function() {
      $('.datepicker').pickadate();
    });
  }

}
