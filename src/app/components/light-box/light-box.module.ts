import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LightBoxComponent } from './light-box.component';
// ********************** angular-modal-gallery *****************************
import 'hammerjs'; // <------ mandatory dependency for angular-modal-gallery
import 'mousetrap'; // <------ mandatory dependency for angular-modal-gallery
import { ModalGalleryModule } from 'angular-modal-gallery';
// <----------------- angular-modal-gallery library import

@NgModule({
  imports: [
    CommonModule,
    ModalGalleryModule.forRoot()
  ],
  declarations: [LightBoxComponent],
  exports: [LightBoxComponent]
})
export class LightBoxModule { }
