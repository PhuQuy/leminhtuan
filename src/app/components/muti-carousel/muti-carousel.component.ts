import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';

@Component({
  selector: 'component-muti-carousel',
  templateUrl: './muti-carousel.component.html',
  styleUrls: ['./muti-carousel.component.css']
})
export class MutiCarouselComponent implements OnInit {
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;

  @Input() items: Array<any>;
  @Input() type: boolean;
  @Input() title: boolean;

  @ViewChild('prevButton') prevButton: ElementRef;
  @ViewChild('nextButton') nextButton: ElementRef;

  constructor() {
    this.carouselTile = {
      grid: { xs: 3, sm: 2, md: 3, lg: 3, all: 0 },
      slide: 1,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true,
      easing: 'ease'
    }
  }

  ngOnInit() {
    this.carouselTileItems = this.items;
  }

  prev() {
    this.prevButton.nativeElement.click();
  }

  next() {
    this.nextButton.nativeElement.click();
  }
}
