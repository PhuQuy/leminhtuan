import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MutiCarouselComponent } from './muti-carousel.component';
import { NgxCarouselModule } from 'ngx-carousel';
import { TransferModule } from '@components/transfer/transfer.module';

@NgModule({
  imports: [
    CommonModule,
    NgxCarouselModule,
    TransferModule
  ],
  declarations: [MutiCarouselComponent],
  exports: [MutiCarouselComponent]
})
export class MutiCarouselModule { }
