import { Component, OnInit, Input } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserService } from '@services/user/user.service';

declare var $: any;
@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  providers: [UserService, AngularFireAuth]
})
export class NavigationComponent implements OnInit {
  @Input() is_root: boolean = false;
  user: any;

  constructor(public afAuth: AngularFireAuth, protected userService: UserService) {
    this.afAuth.authState.subscribe((auth) => {
      if (auth) {
        this.userService.getByElement('uid', auth.uid).subscribe(users => {
          this.user = users[0];
        })
      }
    });
  }

  logout() {
    this.afAuth.auth.signOut();
    location.reload();
  }

  ngOnInit() {
    $('[data-toggle="popover"]').popover();
  }
}
