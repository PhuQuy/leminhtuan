import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThumbnailsCarouselComponent } from './thumbnails-carousel.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ThumbnailsCarouselComponent],
  exports: [ThumbnailsCarouselComponent]
})
export class ThumbnailsCarouselModule { }
