import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'component-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {
  @Input() transferApartment: any;
  constructor() { }

  ngOnInit() {
  }

}
