import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TransferComponent } from './transfer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [TransferComponent],
  exports: [TransferComponent]
})
export class TransferModule { }
