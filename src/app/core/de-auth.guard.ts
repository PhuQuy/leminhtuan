import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class DeAuthGuard implements CanActivate {
  authState: any = null;
  constructor(private afAuth: AngularFireAuth, private router: Router) { }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth
    });

    if(this.authState === null) {
      return true;
    } else {
      console.log('Access Denied');
      this.router.navigate(['/']);
      return false;
    }
  }
}
