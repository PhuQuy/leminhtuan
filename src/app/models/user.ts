export class User {
  $id: string;
  firstName: string;
  lastName: string;
  email: string;
  emailVerified: boolean;
  isAnonymous: boolean;
  phoneNumber: string;
  address: string;
  uid: string;

  constructor() {}

}
