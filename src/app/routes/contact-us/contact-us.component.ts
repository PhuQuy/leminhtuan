import { Component, OnInit } from '@angular/core';
import { ContactService } from '@services/contact/contact.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css'],
  providers: [ContactService]
})
export class ContactUsComponent implements OnInit {

  contact: any = new Object();
  constructor(protected contactService: ContactService) { }

  create() {
    this.contactService.create(this.contact).then(() => {
      console.log("Create success");
    }, error => {
      console.log("Error");
    });
  }

  ngOnInit() {
  }

}
