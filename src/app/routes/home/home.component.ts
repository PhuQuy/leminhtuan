import { Component, OnInit } from '@angular/core';
import { ApartmentService } from '@services/apartment/apartment.service';
import { TransferService } from '@services/transfer/transfer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ApartmentService, TransferService]
})
export class HomeComponent implements OnInit {
  // apartments = [
  //   {
  //     $id: "scenia-bay",
  //     category: "Scenia Bay",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     address: "25-26 Phạm Văn Đồng, P.Vĩnh Hải – Tp. Nha Trang",
  //     area: 65.75,
  //     image: 'assets/img/img51.jpg',
  //     rooms: 2
  //   },
  //   {
  //     $id: "panorama-nha-trang",
  //     category: "Panorama Nha Trang",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "Số 2 Nguyễn Thị Minh Khai, p.Lộc Thọ - TP Nha Trang",
  //     image: 'assets/img/img61.jpg',
  //     rooms: 2
  //   },
  //   {
  //     $id: "a&b-central-square",
  //     category: "A&B Central Square",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "44 Trần Phú, P.Lộc Thọ - Tp. Nha Trang",
  //     image: 'assets/img/img71.jpg',
  //     rooms: 2
  //   },
  //   {
  //     $id: "panorama-nha-trang",
  //     category: "A&B Central Square",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "44 Trần Phú, P.Lộc Thọ - Tp. Nha Trang",
  //     image: 'assets/img/img81.jpg',
  //     rooms: 2
  //   }, {
  //     $id: "panorama-nha-trang-2",
  //     category: "Panorama Nha Trang",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "Số 2 Nguyễn Thị Minh Khai, p.Lộc Thọ - TP Nha Trang",
  //     image: 'assets/img/img51.jpg',
  //     rooms: 2
  //   }, {
  //     $id: "scenia-bay-2",
  //     category: "Scenia Bay",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     address: "25-26 Phạm Văn Đồng, P.Vĩnh Hải – Tp. Nha Trang",
  //     area: 65.75,
  //     image: 'assets/img/img61.jpg',
  //     rooms: 2
  //   }
  // ];
  //
  //
  //
  protected apartment = {
      $id: "Scenia-Bay",
      category: "Scenia Bay",
      name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
      transferPrice: 111000000,
      depositPrice: "512 Triệu",
      address: "25-26 Phạm Văn Đồng, P.Vĩnh Hải – Tp. Nha Trang",
      area: 65.75,
      image: 'assets/img/img61.jpg',
      rooms: 2
    }

  protected apartmentDetails = {
    $id: "nha-trang-city-central",
    name: "Nha Trang City Central",
    address: "29 Phan Chu Trinh, Phường Vạn Thạnh, TP Nha Trang",
    photo: "assets/img/canho.png",
    apartmentCode: ["B01", "D02", "D03", "D04", "A05", "C06", "E07"],
    apartmentItems:[
      {
        date: "08:00 16-03-2018",
        code: 'NTCT-01',
        location: '20-25',
        price: '23,5tr',
        transferPrice: '860,000,000 vnd'
      },
      {
        date: "08:00 16-03-2018",
        code: 'NTCT-01',
        location: '20-25',
        price: '23,5tr',
        transferPrice: '860,000,000 vnd'
      },
      {
        date: "08:00 16-03-2018",
        code: 'NTCT-01',
        location: '20-25',
        price: '23,5tr',
        transferPrice: '860,000,000 vnd'
      },
      {
        date: "08:00 16-03-2018",
        code: 'NTCT-01',
        location: '20-25',
        price: '23,5tr',
        transferPrice: '860,000,000 vnd'
      }
    ],
    status: 'Sắp mở bán',
    investors: 'Đất Xanh Group',
    type: 'Căn hộ',
    blockNumber: 12,
    floorNo: 34,
    apartmentNo: 3175,
    description: 'One Hundred East Fifty Third Street by Foster + Partners is a limited collection of modern residences in Midtown Manhattan Cultural District. The 94 residences ranging from alcove lofts to four bedrooms within the 63-story tower are generously proportioned and seamlessly integrated with infrastructure for modern cosmopolitan living. Acclaimed Michelin-starred French Chef Joel Robuchon will occupy the first two levels',
    images: [
      'http://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(40).jpg',
      'http://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(148).jpg',
      'http://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(145).jpg',
      'http://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(41).jpg',
      'http://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(150).jpg'
    ],
    details: {
      code: '3812',
      bedrooms: 2,
      area: 74.3,
      status: 'Nội thất cơ bản',
      ownership: 'Sổ hồng - được nhập khẩu',
      deadline: 'Quý I 2019',
      price: 29411358,
      total: 2185263900,
      depositPercent: 20,
      discountPercent: 2,
      giatrichenhlech: 30000000,
      phisangnhuong: 5000000,
      giaban: 29411358,
      tonggiatri: 2185263900
    },
    tienich: {

    },
    chudautu: {

    },
    location: {
      lat: 12.2702649,
      lng: 109.2021127,
      // address: '25-26 Nguyễn Đình Chiểu, P.Vĩnh Phước - Tp. Nha Trang'
    },
    videos: {
      link: 'https://www.youtube.com/embed/dArMjjm6qi0',
      description: 'Vị trí dự án  Phối cảnh tổng thể dự án  Hệ thống tiện ích tại dự án Khu vực trung tâm thương mại và shophouse'
    }
  }

  public apartments: any;
  constructor(protected apartmentService: ApartmentService, protected transferService: TransferService) {
    // for(var i = 0, l = this.apartments.length; i < l; i++) {
    //
    //   apartmentService.createCanHo(this.apartments[i]);
    // }
    // apartmentService.createCanHo(this.apartment);
    // transferService.createData(this.apartmentDetails);
    apartmentService.getAll().subscribe(apartments => {
      this.apartments = apartments;
    })
  }

  ngOnInit() {
  }

}
