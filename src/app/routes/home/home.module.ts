import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { CarouselComponent } from '@components/carousel/carousel.component';
import { TransferModule } from '@components/transfer/transfer.module';
import { ApartmentModule } from '@components/apartment/apartment.module';
import { MutiCarouselModule } from '@components/muti-carousel/muti-carousel.module';
import { BannerComponent } from '@components/banner/banner.component';
import { InvestmentComponent } from '@components/investment/investment.component';
import { NewsModule } from '@components/news/news.module';
import { ApartmentForRentModule } from '@components/apartment-for-rent/apartment-for-rent.module';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    ApartmentModule,
    MutiCarouselModule,
    NewsModule,
    TransferModule,
    ApartmentForRentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeComponent, CarouselComponent, BannerComponent, InvestmentComponent]
})
export class HomeModule { }
