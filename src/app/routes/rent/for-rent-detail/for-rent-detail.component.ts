import { Component, OnInit } from '@angular/core';
import { RentService } from '@services/rent/rent.service';
import { ActivatedRoute } from "@angular/router";
import { ApartmentService } from '@services/apartment/apartment.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
  selector: 'app-for-rent-detail',
  templateUrl: './for-rent-detail.component.html',
  styleUrls: ['./for-rent-detail.component.css'],
  providers: [RentService, ApartmentService]
})
export class ForRentDetailComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  ahihi = [1, 2, 3, 4, 5];
  rent: any;
  public lat: number = 12.2702649;
  public lng: number = 109.2021127;
  public address = "25-26 Nguyễn Đình Chiểu, P.Vĩnh Phước - Tp. Nha Trang";
  public zoom = 16;
  apartment: any;
  constructor(protected rentService: RentService, private activatedRoute: ActivatedRoute, protected apartmentService: ApartmentService) {
    this.activatedRoute.params.subscribe(params => {
      if (params['key']) {
        apartmentService.getById(params['key']).subscribe(apartment => {
          this.apartment = apartment;
        });
        rentService.getById(params['key']).subscribe(rent => {
          this.rent = rent;
        });
      }
    });
  }

  ngOnInit() {
    this.galleryOptions = [
           {
               width: '100%',
               height: '500px',
               thumbnailsColumns: 3,
               imageAnimation: NgxGalleryAnimation.Slide
           },
           // max-width 800
           {
               breakpoint: 800,
               width: '50%',
               height: '600px',
               imagePercent: 10,
               thumbnailsColumns: 6,
               thumbnailsPercent: 10,
               thumbnailsMargin: 30,
               thumbnailMargin: 30
           },
           // max-width 400
           {
               breakpoint: 400,
               preview: false
           }
       ];

       this.galleryImages = [
           {
               small: 'assets/img/image5.jpg',
               medium: 'assets/img/image5.jpg',
               big: 'assets/img/image5.jpg'
           },
           {
               small: 'assets/img/image6.jpg',
               medium: 'assets/img/image6.jpg',
               big: 'assets/img/image6.jpg'
           },
           {
               small: 'assets/img/image7.jpg',
               medium: 'assets/img/image7.jpg',
               big: 'assets/img/image7.jpg'
           }
       ];
  }

}
