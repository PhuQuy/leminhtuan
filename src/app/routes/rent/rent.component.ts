import { Component, AfterViewInit } from '@angular/core';
import { ApartmentService } from '@services/apartment/apartment.service';

declare var $: any;
@Component({
  selector: 'app-rent',
  templateUrl: './rent.component.html',
  styleUrls: ['./rent.component.css'],
  providers: [ApartmentService]
})
export class RentComponent implements AfterViewInit {
  public block = '';
  // apartments = [
  //   {
  //     category: "Scenia Bay",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     address: "25-26 Phạm Văn Đồng, P.Vĩnh Hải – Tp. Nha Trang",
  //     area: 65.75,
  //     image: 'assets/img/img51.jpg',
  //     rooms: 2
  //   },
  //   {
  //     category: "Panorama Nha Trang",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "Số 2 Nguyễn Thị Minh Khai, p.Lộc Thọ - TP Nha Trang",
  //     image: 'assets/img/img61.jpg',
  //     rooms: 2
  //   },
  //   {
  //     category: "A&B Central Square",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "44 Trần Phú, P.Lộc Thọ - Tp. Nha Trang",
  //     image: 'assets/img/img71.jpg',
  //     rooms: 2
  //   },
  //   {
  //     category: "A&B Central Square",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "44 Trần Phú, P.Lộc Thọ - Tp. Nha Trang",
  //     image: 'assets/img/img81.jpg',
  //     rooms: 2
  //   }, {
  //     category: "Panorama Nha Trang",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     area: 65.75,
  //     address: "Số 2 Nguyễn Thị Minh Khai, p.Lộc Thọ - TP Nha Trang",
  //     image: 'assets/img/img51.jpg',
  //     rooms: 2
  //   }, {
  //     category: "Scenia Bay",
  //     name: "Căn hộ B.01-2x, dự án Scenia Bay, 2 phòng ngủ, View Đ-B",
  //     transferPrice: 111000000,
  //     depositPrice: "512 Triệu",
  //     address: "25-26 Phạm Văn Đồng, P.Vĩnh Hải – Tp. Nha Trang",
  //     area: 65.75,
  //     image: 'assets/img/img61.jpg',
  //     rooms: 2
  //   }
  // ];

  public apartments: any;
  constructor(protected apartmentService: ApartmentService) {
    $("document").ready(function() {
      $('.dropdown-menu').on('click', function(e) {
        if ($(this).hasClass('dropdown-menu-form')) {
          e.stopPropagation();
        }
      });
    });
    apartmentService.getByElement('type', 'cho-thue').subscribe(apartments => {
      console.log(apartments);
      this.apartments = apartments;
    });
  }

  ngOnInit() {
  }

  check() {
    this.block = 'block';
  }
  ngAfterViewInit() {
    // new WOW().init();
    $('.mdb-select').material_select();
    $('.datepicker').pickadate();
  }
}
