import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RentComponent } from './rent.component';
import { TransferModule } from '@components/transfer/transfer.module';
import { ApartmentModule } from '@components/apartment/apartment.module';
import { ApartmentForRentModule} from '@components/apartment-for-rent/apartment-for-rent.module';
import { ApartmentForSaleModule} from '@components/apartment-for-sale/apartment-for-sale.module';
import { DateTimePickerModule } from '@components/date-time-picker/date-time-picker.module';
import { ForRentDetailComponent } from './for-rent-detail/for-rent-detail.component';
import { MutiCarouselModule } from '@components/muti-carousel/muti-carousel.module';
import { LightBoxModule } from '@components/light-box/light-box.module';
import { BingMapModule } from '@components/bing-map/bing-map.module';
import { ThumbnailsCarouselModule } from '@components/thumbnails-carousel/thumbnails-carousel.module'
import { NgxGalleryModule } from 'ngx-gallery';

const routes: Routes = [
  {
    path: '', component: RentComponent
  },
  {
    path: ':key', component: ForRentDetailComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    TransferModule,
    ApartmentModule,
    ApartmentForRentModule,
    DateTimePickerModule,
    ApartmentForSaleModule,
    MutiCarouselModule,
    LightBoxModule,
    BingMapModule,
    NgxGalleryModule,
    ThumbnailsCarouselModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RentComponent, ForRentDetailComponent]
})
export class RentModule { }
