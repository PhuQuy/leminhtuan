import { Component, AfterViewInit, Input, ViewChild } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';
import { TransferService } from '@services/transfer/transfer.service';
import { Inject, HostListener } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";
import { TransferTabService } from '@services/transfer/transfer-tab.service';
import { EmbedVideoService } from 'ngx-embed-video';
import { ActivatedRoute } from "@angular/router";
import { ApartmentService } from '@services/apartment/apartment.service';
import { ProjectService } from '@services/project/project.service';

declare var $: any;
declare var WOW: any;
@Component({
  selector: 'app-transfer-apartment-detail',
  templateUrl: './transfer-apartment-detail.component.html',
  styleUrls: ['./transfer-apartment-detail.component.css'],
  providers: [TransferService, TransferTabService, EmbedVideoService, ApartmentService, ProjectService]
})

export class TransferApartmentDetailComponent implements AfterViewInit {
//  @Input() apartment: any;
  @ViewChild('giohang') giohang: any;
  @ViewChild('canho') canho: any;
  @ViewChild('location') location: any;
  @ViewChild('tienich') tienich: any;
  @ViewChild('phaply') phaply: any;
  @ViewChild('tintuc') tintuc: any;
  public fixed: boolean = false;
  public iframe_html: any;
  youtubeUrl: any;

  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;
  public lat: number = 12.2702649;
  public lng: number = 109.2021127;
  public address = "25-26 Nguyễn Đình Chiểu, P.Vĩnh Phước - Tp. Nha Trang";
  public selected = "true";

  public zoom = 16;
  items = [
    {
      image: 'assets/img/camranh.jpg'
    },
    {
      image: 'assets/img/camranh1.jpg'
    },
    {
      image: 'assets/img/camranh2.jpg'
    },
    {
      image: 'assets/img/camranh4.jpg'
    }
  ];
  detail: any;
  apartment: any;
  project: any;
  id: string;
  apartmentRows: any;
  constructor(protected transferService: TransferService, protected transferTabService: TransferTabService,
    @Inject(DOCUMENT) private document: Document, private embedService: EmbedVideoService, private activatedRoute: ActivatedRoute,
     private apartmentService: ApartmentService, protected projectService: ProjectService) {
    this.carouselTile = {
      grid: { xs: 3, sm: 2, md: 2.5, lg: 2.5, all: 0 },
      slide: 1,
      speed: 400,
      interval: 3000,
      animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      loop: true,
      touch: true,
      easing: 'ease'
    }
    this.activatedRoute.params.subscribe(params => {
      if (params['key']) {
        this.id = params['key'];
        apartmentService.getById(this.id).subscribe(apartment => {
          this.apartment = apartment;
          if(apartment)
          this.projectService.getById(apartment.duan).subscribe(project =>{
            this.project = project;
          })
        });
        transferService.getById(this.id).subscribe(detail => {
          this.detail = detail;
          this.iframe_html = this.embedService.embed(detail.videos.link, { query: { portrait: 0, color: '333' }, attr: { width: 500, height: 300 } });
          transferTabService.getByElement('macan', detail.apartmentCode[0]).subscribe(apartmentRows => {
            this.apartmentRows = apartmentRows;
          });
        });
      }
    })
    // for(var i = 0, l = this.transferTabData.length; i < l; i++) {
    //
    //   transferTabService.create(this.transferTabData[i]);
    // }
  }

  selectCode(code) {
    this.transferTabService.getByElement('macan', code).subscribe(apartmentRows => {
      this.apartmentRows = apartmentRows;
    });

  }

  ngOnInit() {
    this.carouselTileItems = this.items;
    this.scrollTo(0);
    $('[data-toggle="tooltip"]').tooltip();
    // $(document).ready(function() {
    //   $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
    // });
  }

  public scrollTo(value) {
    $('body,html').animate({
      scrollTop: value
    }, 300, 'swing');
  }

  public scrollTolocation() {
    $('body,html').animate({
      scrollTop: this.location.nativeElement.offsetTop - 15
    }, 300, 'swing');
  }

  public scrollToGioHang() {
    $('body,html').animate({
      scrollTop: this.giohang.nativeElement.offsetTop - 15
    }, 300, 'swing');
  }

  public scrollToCanHo() {
    $('body,html').animate({
      scrollTop: this.canho.nativeElement.offsetTop - 15
    }, 300, 'swing');
  }

  public scrollToLocation() {
    $('body,html').animate({
      scrollTop: this.location.nativeElement.offsetTop - 15
    }, 300, 'swing');
  }

  public scrollToTienIch() {
    $('body,html').animate({
      scrollTop: this.tienich.nativeElement.offsetTop - 15
    }, 300, 'swing');
  }

  public scrollToPhapLy() {
    $('body,html').animate({
      scrollTop: this.phaply.nativeElement.offsetTop - 70
    }, 300, 'swing');
  }

  public scrollToTintuc() {
    $('body,html').animate({
      scrollTop: this.tintuc.nativeElement.offsetTop - 15
    }, 300, 'swing');

  }

  ngAfterViewInit() {
    // new WOW().init();
    // $('.mdb-select').material_select();
  }

  watchVideos() {
    this.scrollTo(500);
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    if (number > 200) {
      this.fixed = true;
    } else if (this.fixed && number < 200) {
      this.fixed = false;
    }
  }
}
