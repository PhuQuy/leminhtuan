import { Component, OnInit } from '@angular/core';
import { ApartmentService } from '@services/apartment/apartment.service';

@Component({
  selector: 'app-transfer-apartment-list',
  templateUrl: './transfer-apartment-list.component.html',
  styleUrls: ['./transfer-apartment-list.component.css'],
  providers: [ApartmentService]
})
export class TransferApartmentListComponent implements OnInit {
  apartments: any;
  constructor(private apartmentService : ApartmentService) {
    apartmentService.getByElement('type', 'chuyen-nhuong').subscribe(apartments => {
      this.apartments = apartments;
    });
  }

  ngOnInit() {
  }

}
