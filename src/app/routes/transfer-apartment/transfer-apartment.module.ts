import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TransferApartmentDetailComponent } from './transfer-apartment-detail/transfer-apartment-detail.component';
import { TransferApartmentListComponent } from './transfer-apartment-list/transfer-apartment-list.component';
import { NgxCarouselModule } from 'ngx-carousel';
import { BingMapModule } from '@components/bing-map/bing-map.module';
import { ApartmentRowComponent } from '@components/apartment-row/apartment-row.component';
import { NewsModule } from '@components/news/news.module';
import { TransferModule } from '@components/transfer/transfer.module';
import { ApartmentForSaleModule} from '@components/apartment-for-sale/apartment-for-sale.module';
import { LightBoxModule } from '@components/light-box/light-box.module';
import { SharedService } from '@services/shared/shared.service';
import { MutiCarouselModule } from '@components/muti-carousel/muti-carousel.module';
import { EmbedVideo } from 'ngx-embed-video';
import { HttpModule } from '@angular/http';

const routes: Routes = [
  {
    path: '', component: TransferApartmentListComponent
  },
  {
    path: ':key', component: TransferApartmentDetailComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    NgxCarouselModule,
    BingMapModule,
    NewsModule,
    TransferModule,
    LightBoxModule,
    ApartmentForSaleModule,
    MutiCarouselModule,
    HttpModule,
    EmbedVideo.forRoot(),
    RouterModule.forChild(routes)
  ],
  providers: [SharedService],
  declarations: [TransferApartmentDetailComponent, ApartmentRowComponent, TransferApartmentListComponent]
})
export class TransferApartmentModule { }
