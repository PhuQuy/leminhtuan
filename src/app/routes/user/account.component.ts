import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '@models/user';
import { UserService } from '@services/user/user.service';

@Component({
  templateUrl: './sign-up/sign-up.component.html'
})
export class AccountComponent implements OnInit {
  public error: any;
  public success: any;
  public name: string;
  public auth: any = null;
  public showSpinner = false;
  public firstName:any;
  public lastName: any;
  public email: any;
  public pass: any;
  public myform: any;
  public user: User = new User();
  public password: string = '';
  authState: any = null;
  // public firstName: string;
  constructor(public afAuth: AngularFireAuth, public router: Router, protected userService: UserService) {
  }

  public closeAlert() {
    this.success = null;
    this.error = null;
  }

  logout() {
    this.afAuth.auth.signOut();
    location.reload();
  }

  signUp() {
    this.showSpinner = true;
    return this.afAuth.auth.createUserWithEmailAndPassword(this.user.email, this.password)
      .then((user) => {
        this.showSpinner = false;
        this.authState = user;
        this.user.uid = user.uid;
        this.userService.create(this.user);
        user.sendEmailVerification().then(function() {
          // Email sent.
        }).catch(function(error) {
          // An error happened.
          this.showSpinner = false;
        });
      })
      .catch(error => {
        this.error = error;
        this.showSpinner = false;
      });
  }

  ngOnInit() {
  }
}
