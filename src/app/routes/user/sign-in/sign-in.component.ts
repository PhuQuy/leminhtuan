import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from "@angular/router";
import { AccountComponent } from './../account.component';
import { UserService } from '@services/user/user.service';

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
  providers: [AngularFireAuth, UserService]
})
export class SignInComponent extends AccountComponent {
  public showSpinner = false;
  public error: any;
  public email: string;
  public password: string;

  loginByEmailAndPassword() {
    this.showSpinner = true;
    this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password).then(
      (success) => {
        this.error = null;
        this.showSpinner = false;
        this.router.navigate(['/']);
      }).catch(
        (err) => {
          this.showSpinner = false;
          this.error = err.message;
        });
  }

  ngOnInit() {
  }

}
