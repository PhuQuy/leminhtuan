import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Injectable()
export class BaseService {
  protected basePath = '';
  constructor(protected angularFirestore: AngularFirestore, path: string) {
    this.basePath = path;
  }

  public getAll(query?) {
    return this.angularFirestore.collection<any>(this.basePath).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  public getById(id) {
    let itemPath = `${this.basePath}/${id}`;
    return this.angularFirestore.doc<any>(itemPath).valueChanges();
  }

  public getByElement(element:string, value:any) {
    return this.angularFirestore.collection(this.basePath, ref => ref.where(element, '==', value)).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as any;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  public create(data) {
    const timestamp = this.timestamp;
    return this.angularFirestore.collection(this.basePath).add({
      ...data, createdAt: timestamp
    });
  }

  public updateOrCreate(data) {
    if(data.id == null) {
      return null;
    }
    const timestamp = this.timestamp;
    return this.angularFirestore.collection(this.basePath).doc(data.id).set({
      ...data, createdAt: timestamp
    });
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

}
