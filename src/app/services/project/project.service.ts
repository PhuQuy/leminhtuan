import { Injectable } from '@angular/core';
import { BaseService } from '@services/base.service';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class ProjectService extends BaseService {

  constructor(protected angularFirestore: AngularFirestore) {
    super(angularFirestore, 'du-an');
  }

}
