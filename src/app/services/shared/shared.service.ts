import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SharedService {

  private emitModelTransferSource = new Subject<any>();
  // Observable string streams
  emitModelTransferSource$ = this.emitModelTransferSource.asObservable();

  emitModelTransferChange(change: any) {
    this.emitModelTransferSource.next(change);
  }

  getEmitModelTransferChangeSource() {
    return this.emitModelTransferSource;
  }


}
