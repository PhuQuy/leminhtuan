import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { BaseService } from '@services/base.service';

@Injectable()
export class TransferTabService extends BaseService {
  constructor(protected angularFirestore: AngularFirestore) {
    super(angularFirestore, 'bang-hang-chuyen-nhuong');
  }

}
